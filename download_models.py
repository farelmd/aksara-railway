from aksara.dependency_parser import DependencyParser

all_models = [
    "FR_GSD-ID_CSUI",
    "FR_GSD-ID_GSD",
    "IT_ISDT-ID_CSUI",
    "IT_ISDT-ID_GSD",
    "EN_GUM-ID_CSUI",
    "EN_GUM-ID_GSD",
]

parser = DependencyParser()

for model in all_models:
    parser.parse('halo', model=model)
