#!/bin/bash
echo "start build"
apt-get update && apt-get -y upgrade
apt-get install -y foma-bin
python3 -m pip install -r requirements.txt
python3 manage.py collectstatic --noinput --clear
echo "end build"