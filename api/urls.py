from django.urls import path
from .views import *

urlpatterns = [
    path('lemmatize', lemmatize_api, name="lemmatize-api"),
    path('pos-tag', pos_tag_api, name="pos-tag-api"),
    path('morphological-feature', morphological_feature_api, name="morphological-feature-api"),
    path('dependency-parse', dependency_parse_api, name="dependency-parse-api"),
    path('based-on-actions', based_on_actions_api, name="based-on-actions-api")
]