import json

from typing import List, Dict
from aksara.conllu import ConlluData
from aksara.core import split_sentence
from aksara.tokenizers import MultiwordTokenizer
from aksara.lemmatizer import Lemmatizer
from aksara.morphological_analyzer import MorphologicalAnalyzer
from aksara.pos_tagger import POSTagger
from aksara.dependency_parser import DependencyParser
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.exceptions import ParseError
from .serializers import ConlluDataSerializer


@api_view(["POST"])
@parser_classes([JSONParser])
def lemmatize_api(request):
    data = request.data

    if "sentences" not in data:
        raise ParseError({"detail": "sentences must be in input json"})

    sentences = str(data.get("sentences"))
    is_informal = bool(data.get("is_informal", False))
    lemmatizer = Lemmatizer()
    tokenizer = MultiwordTokenizer()

    sentences_list = split_sentence(sentences)
    result = []

    for sentence in sentences_list:
        tokenized_sentence = tokenizer.tokenize(sentence)
        lemmatize_result = lemmatizer.lemmatize_batch(tokenized_sentence, is_informal=is_informal)
        result.append(lemmatize_result)

    return Response(
        {"result": result}
    )


@api_view(["POST"])
@parser_classes([JSONParser])
def pos_tag_api(request):
    data = request.data


    if "sentences" not in data:
        raise ParseError({"detail": "sentences must be in input json"})

    sentences = str(data.get("sentences"))
    is_informal = bool(data.get("is_informal", False))
    sep_regex = data.get("sep_regex", None)
    tagger = POSTagger()

    result = tagger.tag(sentences, is_informal=is_informal, sep_regex=sep_regex)

    return Response(
        {"result": result}
    )


@api_view(["POST"])
@parser_classes([JSONParser])
def morphological_feature_api(request):
    data = request.data

    if "sentences" not in data:
        raise ParseError({"detail": "sentences must be in input json"})

    sentences = str(data.get("sentences"))
    is_informal = bool(data.get("is_informal", False))
    sep_regex = data.get("sep_regex", None)
    feature = MorphologicalAnalyzer()

    result = feature.analyze(sentences, is_informal=is_informal, sep_regex=sep_regex)

    return Response(
        {"result": result}
    )


@api_view(["POST"])
@parser_classes([JSONParser])
def dependency_parse_api(request):
    data = request.data

    if "sentences" not in data:
        raise ParseError({"detail": "sentences must be in input json"})

    sentences = str(data.get("sentences"))
    is_informal = bool(data.get("is_informal", False))
    sep_regex = data.get("sep_regex", None)
    model = data.get("model", "FR_GSD-ID_CSUI")
    parser = DependencyParser()

    try:
        conllu_lists = parser.parse(sentences, is_informal=is_informal, sep_regex=sep_regex, model=model)
        result = __serialize_conllu_lists(conllu_lists)

        return Response(
            {"result": result}
        )
    except ValueError as e:
        raise ParseError({"detail": f"{str(e)}"})


@api_view(["POST"])
@parser_classes([JSONParser])
def based_on_actions_api(request):
    data = request.data

    if "sentences" not in data:
        raise ParseError({"detail": "sentences must be in input json"})

    sentences = str(data.get("sentences"))
    is_informal = bool(data.get("is_informal", False))
    sep_regex = data.get("sep_regex", None)
    model = data.get("model", "FR_GSD-ID_CSUI")

    actions = request.GET.get("actions", "lemma,tag,feat,parse")

    parser = DependencyParser()

    try:
        conllu_lists = parser.parse(
            sentences, is_informal=is_informal, sep_regex=sep_regex, model=model
        )
        result = __serialize_conllu_lists(conllu_lists, actions)

        return Response(
            {"result": result}
        )
    except ValueError as e:
        raise ParseError({"detail": f"{str(e)}"})


def __serialize_conllu_lists(
        conllu_lists: List[List[ConlluData]],
        actions: str = "lemma,tag,feat,parse"
) -> List[List[Dict]]:

    action_list = [action.strip().lower() for action in actions.split(",")]

    is_lemma = "lemma" in action_list
    is_tag = "tag" in action_list
    is_feat = "feat" in action_list
    is_parse = "parse" in action_list

    result = []
    for sentence in conllu_lists:
        sentence_result = []
        for conllu in sentence:
            temp = ConlluDataSerializer(conllu, is_lemma, is_tag, is_feat, is_parse)
            sentence_result.append(temp.data)
        result.append(sentence_result)

    return result
