from aksara.conllu import ConlluData


class ConlluDataSerializer:
    def __init__(
            self, conllu_data: ConlluData, is_lemma: bool = True,
            is_tag: bool = True, is_feat: bool = True,
            is_parse: bool = True
    ):
        idx = conllu_data.get_id()
        form = conllu_data.get_form()
        lemma = conllu_data.get_lemma() if is_lemma else "_"
        upos = conllu_data.get_upos() if is_tag else "_"
        xpos = conllu_data.get_xpos()
        feat = conllu_data.get_feat() if is_feat else "_"
        head_id = conllu_data.get_head_id() if is_parse else "_"
        deprel = conllu_data.get_deprel() if is_parse else "_"

        self.data = {
            "idx": idx, "form": form, "lemma": lemma,
            "upos": upos, "xpos": xpos, "feat": feat,
            "head_id": head_id, "deprel": deprel
        }
