import json
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class MorphologicalAnalyzeAPITest(APITestCase):

    def setUp(self) -> None:
        self.endpoint = reverse('morphological-feature-api')
        return super().setUp()

    def test_morphological_feature_api_success_contains_correct_body(self):
        request_data = {
            "sentences": "Seluruh pakaian saya sudah kering. Kemarin matahari bersinar terang.",
            "is_informal": False,
            "sep_regex": None,
        }

        response = self.client.post(self.endpoint, json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            {"result":
                [
                    [
                        ("Seluruh", []),
                        ("pakaian", ["Number=Sing"]),
                        ("saya", ["Number=Sing", "Person=1", "PronType=Prs"]),
                        ("sudah", []),
                        ("kering", []),
                        (".", [])
                    ],
                    [
                        ("Kemarin", ["Number=Sing"]),
                        ("matahari", ["Number=Sing"]),
                        ("bersinar", ["Voice=Act"]),
                        ("terang", []),
                        (".", [])
                    ]
                ]
             }
        )

    def test_morphological_feature_api_success_if_sentences_is_not_string(self):
        # anything in sentences will be converted to a string using str()
        request_data = {
            "sentences": [1, 2, 3]
        }

        response = self.client.post(self.endpoint, json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_morphological_feature_api_success_if_is_informal_is_not_boolean(self):
        # anything in is_informal will be converted to a boolean using bool()
        request_data = {
            "sentences": "Seluruh pakaian saya sudah kering. Kemarin matahari bersinar terang.",
            "is_informal": [1,2,3]
        }

        response = self.client.post(self.endpoint, json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_morphological_feature_api_fails_if_sentences_is_not_in_input_json(self):
        request_data = {
            "is_informal": False
        }

        response = self.client.post(self.endpoint, json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {"detail": "sentences must be in input json"}
        )

    def test_morphological_feature_api_fails_if_input_is_not_json(self):
        request_data = "Seluruh pakaian saya sudah kering. Kemarin matahari bersinar terang."

        response = self.client.post(self.endpoint, request_data,
                                    content_type="text/html")
        self.assertEqual(response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def test_morphological_feature_api_fails_if_using_get_method(self):
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
