from unittest import TestCase
from aksara.conllu import ConlluData
from api.serializers import ConlluDataSerializer


class ConlluDataSerializerTest(TestCase):
    def setUp(self) -> None:
        # all data used is dummy
        self.conllu_attributes = {
            "idx": "1", "form": "Apa", "lemma": "apa",
            "upos": "PRON", "xpos": "X", "feat": "X",
            "head_id": "0", "deprel": "root"
        }

        self.conllu = ConlluData(**self.conllu_attributes)
        self.serializer = ConlluDataSerializer(self.conllu)

    def test_contains_expected_fields(self):
        data = self.serializer.data
        expected = ['idx', 'form', 'lemma', 'upos', 'xpos', 'feat', 'head_id', 'deprel']

        self.assertCountEqual(data.keys(), expected)

    def test_data_is_correct(self):
        data = self.serializer.data

        self.assertEqual(data, self.conllu_attributes)

    def test_lemma_is_removed_if_is_lemma_is_false(self):
        serialize = ConlluDataSerializer(self.conllu, is_lemma=False)
        data = serialize.data
        self.conllu_attributes["lemma"] = "_"

        self.assertEqual(data, self.conllu_attributes)

    def test_upos_is_removed_if_is_tag_is_false(self):
        serialize = ConlluDataSerializer(self.conllu, is_tag=False)
        data = serialize.data
        self.conllu_attributes["upos"] = "_"

        self.assertEqual(data, self.conllu_attributes)

    def test_feat_is_removed_if_is_feat_is_false(self):
        serialize = ConlluDataSerializer(self.conllu, is_feat=False)
        data = serialize.data
        self.conllu_attributes["feat"] = "_"

        self.assertEqual(data, self.conllu_attributes)

    def test_head_id_and_deprel_is_removed_if_is_parse_is_false(self):
        serialize = ConlluDataSerializer(self.conllu, is_parse=False)
        data = serialize.data
        self.conllu_attributes["head_id"] = "_"
        self.conllu_attributes["deprel"] = "_"

        self.assertEqual(data, self.conllu_attributes)
