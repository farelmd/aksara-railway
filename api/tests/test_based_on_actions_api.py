import json
from rest_framework import status
from rest_framework.test import APITestCase


class POSTagAPITest(APITestCase):

    def setUp(self) -> None:
        self.result = {
            "result":
                [
                    [
                        {
                            "idx": "1", "form": "Apa", "lemma": "apa",
                            "upos": "PRON", "xpos": "_", "feat": "_",
                            "head_id": "0", "deprel": "root"
                        },
                        {
                            "idx": "2", "form": "yang", "lemma": "yang",
                            "upos": "SCONJ", "xpos": "_", "feat": "_",
                            "head_id": "4", "deprel": "mark"
                        },
                        {
                            "idx": "3", "form": "kamu", "lemma": "kamu",
                            "upos": "PRON", "xpos": "_", "feat": "Number=Sing|Person=2|PronType=Prs",
                            "head_id": "4", "deprel": "nsubj"
                        },
                        {
                            "idx": "4", "form": "inginkan", "lemma": "ingin",
                            "upos": "VERB", "xpos": "_", "feat": "Voice=Act",
                            "head_id": "1", "deprel": "acl"
                        },
                        {
                            "idx": "5", "form": "?", "lemma": "?",
                            "upos": "PUNCT", "xpos": "_", "feat": "_",
                            "head_id": "4", "deprel": "punct"
                        }
                    ],
                    [
                        {
                            "idx": "1", "form": "Saya", "lemma": "saya",
                            "upos": "PRON", "xpos": "_", "feat": "Number=Sing|Person=1|PronType=Prs",
                            "head_id": "2", "deprel": "nsubj"
                        },
                        {
                            "idx": "2", "form": "ingin", "lemma": "ingin",
                            "upos": "VERB", "xpos": "_", "feat": "_",
                            "head_id": "0", "deprel": "root"
                        },
                        {
                            "idx": "3", "form": "makan", "lemma": "makan",
                            "upos": "VERB", "xpos": "_", "feat": "_",
                            "head_id": "2", "deprel": "xcomp"
                        },
                        {
                            "idx": "4", "form": ".", "lemma": ".",
                            "upos": "PUNCT", "xpos": "_", "feat": "_",
                            "head_id": "3", "deprel": "punct"
                        },
                    ]
                ]
        }
        return super().setUp()

    def test_based_on_actions_api_success_contains_correct_body(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.result)

    def test_default_value_actions_is_lemma_tag_feat_parse(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.result)

    def test_lemma_is_removed_if_there_is_no_lemma_in_actions(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions?actions=tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = response.data.get("result")
        lemmas = [conllu['lemma'] for sentence in result for conllu in sentence]
        uposes = [conllu['upos'] for sentence in result for conllu in sentence]
        feats = [conllu['feat'] for sentence in result for conllu in sentence]
        head_ids = [conllu['head_id'] for sentence in result for conllu in sentence]
        deprels = [conllu['deprel'] for sentence in result for conllu in sentence]

        self.assertEqual(set("_"), set(lemmas))
        self.assertNotEqual(set("_"), set(uposes))
        self.assertNotEqual(set("_"), set(feats))
        self.assertNotEqual(set("_"), set(head_ids))
        self.assertNotEqual(set("_"), set(deprels))

    def test_upos_is_removed_if_there_is_no_tag_in_actions(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = response.data.get("result")
        lemmas = [conllu['lemma'] for sentence in result for conllu in sentence]
        uposes = [conllu['upos'] for sentence in result for conllu in sentence]
        feats = [conllu['feat'] for sentence in result for conllu in sentence]
        head_ids = [conllu['head_id'] for sentence in result for conllu in sentence]
        deprels = [conllu['deprel'] for sentence in result for conllu in sentence]

        self.assertNotEqual(set("_"), set(lemmas))
        self.assertEqual(set("_"), set(uposes))
        self.assertNotEqual(set("_"), set(feats))
        self.assertNotEqual(set("_"), set(head_ids))
        self.assertNotEqual(set("_"), set(deprels))

    def test_feat_is_removed_if_there_is_no_feat_in_actions(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = response.data.get("result")
        lemmas = [conllu['lemma'] for sentence in result for conllu in sentence]
        uposes = [conllu['upos'] for sentence in result for conllu in sentence]
        feats = [conllu['feat'] for sentence in result for conllu in sentence]
        head_ids = [conllu['head_id'] for sentence in result for conllu in sentence]
        deprels = [conllu['deprel'] for sentence in result for conllu in sentence]

        self.assertNotEqual(set("_"), set(lemmas))
        self.assertNotEqual(set("_"), set(uposes))
        self.assertEqual(set("_"), set(feats))
        self.assertNotEqual(set("_"), set(head_ids))
        self.assertNotEqual(set("_"), set(deprels))

    def test_head_id_and_deprel_is_removed_if_there_is_no_parse_in_actions(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": False,
            "sep_regex": None,
            "model": "FR_GSD-ID_CSUI",
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat",
                                    json.dumps(request_data),
                                    content_type="application/json")
        result = response.data.get("result")
        lemmas = [conllu['lemma'] for sentence in result for conllu in sentence]
        uposes = [conllu['upos'] for sentence in result for conllu in sentence]
        feats = [conllu['feat'] for sentence in result for conllu in sentence]
        head_ids = [conllu['head_id'] for sentence in result for conllu in sentence]
        deprels = [conllu['deprel'] for sentence in result for conllu in sentence]

        self.assertNotEqual(set("_"), set(lemmas))
        self.assertNotEqual(set("_"), set(uposes))
        self.assertNotEqual(set("_"), set(feats))
        self.assertEqual(set("_"), set(head_ids))
        self.assertEqual(set("_"), set(deprels))

    def test_based_on_actions_api_success_if_sentences_is_not_string(self):
        # anything in sentences will be converted to a string using str()
        request_data = {
            "sentences": [1, 2, 3]
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_based_on_actions_api_success_if_is_informal_is_not_boolean(self):
        # anything in is_informal will be converted to a boolean using bool()
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "is_informal": [1, 2, 3]
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_based_on_actions_api_fails_if_sentences_is_not_in_input_json(self):
        request_data = {
            "is_informal": False
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {"detail": "sentences must be in input json"}
        )

    def test_based_on_actions_api_fails_if_model_is_unknown(self):
        request_data = {
            "sentences": "Apa yang kamu inginkan? Saya ingin makan.",
            "model": "unknown_model"
        }

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    json.dumps(request_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data.get("detail")),
            "model must be one of ['FR_GSD-ID_CSUI', 'FR_GSD-ID_GSD', 'IT_ISDT-ID_CSUI', \
'IT_ISDT-ID_GSD', 'EN_GUM-ID_CSUI', 'EN_GUM-ID_GSD', 'SL_SSJ-ID_CSUI', \
'SL_SSJ-ID_GSD'], but unknown_model was given"
        )

    def test_based_on_actions_api_fails_if_input_is_not_json(self):
        request_data = "Apa yang kamu inginkan? Saya ingin makan."

        response = self.client.post("/api/based-on-actions?actions=lemma,tag,feat,parse",
                                    request_data,
                                    content_type="text/html")
        self.assertEqual(response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

    def test_based_on_actions_api_fails_if_using_get_method(self):
        response = self.client.get("/api/based-on-actions?actions=lemma,tag,feat,parse")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
